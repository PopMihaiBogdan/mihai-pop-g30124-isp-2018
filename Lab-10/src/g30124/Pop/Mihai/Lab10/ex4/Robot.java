package g30124.Pop.Mihai.Lab10.ex4;

public class Robot extends Thread
{
    private int id;
    private int coordX,coordY;
    private static final int LOWER_BOUND=0;
    private static final int UPPER_BOUND=5;
    private volatile boolean isStopped;

    public Robot(int id)
    {
        this.id=id;
        isStopped=false;
        coordX= (int) (Math.random()*UPPER_BOUND);
        coordY= (int) (Math.random()*UPPER_BOUND);
    }

    public void stopThread()
    {
        isStopped=true;
    }

    public void makeMove()
    {
        coordX++;
        coordY--;
        if(coordX>=UPPER_BOUND)
            coordX=LOWER_BOUND;
        if(coordY<=LOWER_BOUND)
            coordY=UPPER_BOUND;
    }

    public int getCoordX() {
        return coordX;
    }

    public int getCoordY() {
        return coordY;
    }


    public int getIdRobot() {
        return id;
    }

    @Override
    public void run()
    {
        while(!isStopped)
        {
            //System.out.println("salut");
            makeMove();
            try {
                sleep(100);
                synchronized (this)
                {
                    wait();
                }
                //System.out.println("am trecut de wait");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean equals(Object obj) {
        Robot robot=(Robot) obj;
        return this.id==robot.getIdRobot();
    }

    @Override
    public String toString() {
        return "" + coordX + " " +coordY;
    }
}
