package g30124.Pop.Mihai.Lab10.ex4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import static java.lang.Thread.sleep;

public class Controller
{
    ArrayList<Robot> robots;

    public Controller()
    {
        robots=new ArrayList<Robot>();
        startSimulation();
    }

    private void startSimulation()
    {
        for(int i=0;i<10;i++)
        {
            robots.add(new Robot(i));
            robots.get(i).start();
        }

        TreeSet<Robot> toBeRemoved;

        while(robots.size()!=0) {
            for (int i = 0; i < robots.size(); i++) {
                System.out.println("Robot " + i + " " + robots.get(i).toString());
            }
            System.out.println("********************");
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            toBeRemoved = new TreeSet<>(new Comparator<Robot>() {
                @Override
                public int compare(Robot o1, Robot o2) {
                     if(o1.getIdRobot() == o2.getIdRobot())
                         return 0;
                     else
                         return -1;
                }
            });
            for(int i=0;i<robots.size();i++)
            {
                for(int j=i+1;j<robots.size();j++)
                {
                    if(robots.get(i).getCoordX()==robots.get(j).getCoordX() && robots.get(i).getCoordY() == robots.get(j).getCoordY()) {
                        toBeRemoved.add(robots.get(j));
                        toBeRemoved.add(robots.get(i));
                        robots.get(i).stopThread();
                        robots.get(j).stopThread();
                    }
                }
            }
            robots.removeAll(toBeRemoved);

            for(int i=0;i<robots.size();i++) {

                synchronized (robots.get(i))
                {
                    robots.get(i).notify();
                }
            }
        }
    }
}
