package g30124.Pop.Mihai.Lab10.ex3;

public class CounterS extends Thread {

    CounterS(String name) {
        super(name);
    }

    public void run() {
        for (int i = 100; i < 200; i++) {
            System.out.println(getName() + " i = " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
    }
}