package g30124.Pop.Mihai.Lab10.ex2;

class FirGet extends Thread {
    Punct p;

    public FirGet(Punct p){
        this.p = p;
    }

    public void run(){
        int i=0;
        int a,b;
        while(++i<15){
            synchronized(p) {
                a = p.getX();
                b = p.getY();
            try {
                sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
             }
            System.out.println("Am citit: ["+a+","+b+"]");
        }
    }
}

