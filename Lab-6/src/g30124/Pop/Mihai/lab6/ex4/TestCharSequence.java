package g30124.Pop.Mihai.lab6.ex4;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class TestCharSequence {
    private CharSequence charSequence;
    @Before
    public void set()
    {  char[] new_sir;
         new_sir="da";
        charSequence=new CharSequence(new_sir);
    }
    @Test
    public void shouldGetChar()
    {
        assertEquals(charSequence.charAt(2),"o");
    }
}
