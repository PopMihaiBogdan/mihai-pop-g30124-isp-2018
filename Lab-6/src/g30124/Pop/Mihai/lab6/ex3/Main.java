package g30124.Pop.Mihai.lab6.ex3;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1;
        b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,150,100,"1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,4,9,"2",false);
        b1.addShape(s2);
        Shape s3=new Rectangle(Color.BLUE,45,40,90,"3",true);
        b1.addShape(s3);
        //b1.deleteById("1");

    }
}