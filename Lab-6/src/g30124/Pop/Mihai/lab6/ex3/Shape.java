package g30124.Pop.Mihai.lab6.ex3;

import java.awt.*;

public  interface  Shape {
    void draw(Graphics g);
    String getId();
}
