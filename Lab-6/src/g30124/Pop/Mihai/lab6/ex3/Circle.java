package g30124.Pop.Mihai.lab6.ex3;

import java.awt.*;

public class Circle implements Shape{

    private int radius,x,y;
    private String id;
    boolean isFilled;
    private Color color;

    @Override
    public String getId() {
        return id;
    }

    public Circle(Color color, int radius, int x, int y, String id, boolean isFilled) {
        this.color=color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.isFilled=isFilled;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }


    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius);
        g.setColor(color);
        g.drawOval(x,y,radius,radius);
        if(isFilled==true)
            g.fillOval(x,y,radius,radius);

    }

}