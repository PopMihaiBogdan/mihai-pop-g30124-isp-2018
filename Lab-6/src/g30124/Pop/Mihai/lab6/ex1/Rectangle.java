package g30124.Pop.Mihai.lab6.ex1;


import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color, int length,int x,int y,String id,boolean isFilled) {
        super(color,x,y,id,isFilled);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.drawRect(super.getX(),super.getY(),length,length);
        if(super.isFilled()==true)
            g.fillRect(super.getX(),super.getY(),length,length);
    }

}