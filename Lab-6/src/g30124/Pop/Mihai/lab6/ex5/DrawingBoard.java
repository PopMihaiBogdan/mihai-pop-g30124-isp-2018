package g30124.Pop.Mihai.lab6.ex5;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard  extends JFrame {

    Pyramid[] pyramids = new Pyramid[100];
    //ArrayList<Shape> shapes = new ArrayList<>();

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(800,800);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    public void addPyramid(Pyramid p1){
        for(int i=0;i<pyramids.length;i++){
            if(pyramids[i]==null){
                pyramids[i] = p1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<pyramids.length;i++){
            if(pyramids[i]!=null)
                pyramids[i].draw(g);
        }
//        for(Shape s:shapes)
//            s.draw(g);
    }
}