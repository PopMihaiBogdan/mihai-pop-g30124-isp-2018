package g30124.Pop.Mihai.lab6.ex5;

import java.awt.*;
import java.util.Scanner;

public class Main {
    public static int getBaseLength(int n)
    {

      int x=(int)(-1+Math.sqrt(1+8*n))/2;
      return x;
    }
    public static void main(String[] args) {
     int n,height=40,width=60,indentare=0,level=0;
        Scanner input =new Scanner(System.in);
        System.out.println("Enter the total number of bricks");

        n=input.nextInt();
        int base= getBaseLength(n);

        DrawingBoard board = new DrawingBoard();
        Pyramid p1;
        for(int i=base;i>=1;i--) {
            indentare=0;
            for(int j=base;j>=base-i+1;j--)
            {p1 = new Pyramid(height,width,40+indentare*width+level,60+height*i);
                board.addPyramid(p1);
            indentare++;
            }
            level+=width/2;

        }

    }
}
