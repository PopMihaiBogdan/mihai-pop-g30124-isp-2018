package g30124.Pop.Mihai.lab7.ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException{
        Dictionary d = new Dictionary();
        char comanda;
        String citire, descriere ;
        BufferedReader fluxIntrare = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Meniu");
            System.out.println("1 - Adauga un cuvant");
            System.out.println("2 - Cauta un cuvant");
            System.out.println("3 - Listeaza dictionarul");
            System.out.println("4 - Iesire");

            citire = fluxIntrare.readLine();
            comanda = citire.charAt(0);

            switch(comanda) {
                case '1':
                    System.out.println("Introduceti cuvantul: ");
                    citire = fluxIntrare.readLine();
                    Word w = new Word(citire);
                    w.setName(citire);
                    if(citire.length()>1) {
                        System.out.println("Introduceti definitia: ");
                        descriere = fluxIntrare.readLine();
                        Definition des = new Definition(descriere);
                        d.addWord(w, des);
                    }
                    break;

                case '2':
                    System.out.println("Introduceti cuvantul cautat: ");
                    citire = fluxIntrare.readLine();
                    Word w1 = new Word(citire);
                    w1.setName(citire);
                    if(citire.length()>1) {
                        if(d.getDefinition(w1) == null)
                            System.out.println("Nu exista!");
                        else
                            System.out.println("Explicatie: " +d.getDefinition(w1));
                    }
                    break;

                case '3':
                    System.out.println("Afiseaza: ");
                    d.getAllWords();
                    d.getAllDefinitions();
                    break;
            }
        }while(comanda != '4');
        System.out.println("Programul s-a incheiat!");
    }

}