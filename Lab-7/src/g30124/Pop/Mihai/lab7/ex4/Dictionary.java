package g30124.Pop.Mihai.lab7.ex4;

import java.util.*;

public class Dictionary {

    HashMap<Word,Definition> dictionary = new HashMap<Word, Definition>();

    public void addWord(Word w, Definition d) {
        dictionary.put(w, d);

    }

    public Definition getDefinition(Word w) {
        if(dictionary.containsKey(w)) {
            System.out.println(dictionary.get(w).getDescription());
            System.out.println(dictionary.get(w));
            return dictionary.get(w);
        }
        System.out.println("Cuvantul " + w + " nu a fost gasit.");
        return null;
    }

    public String getDefinitions(Word w) {
        if(dictionary.containsKey(w)) {
            System.out.println(dictionary.get(w).getDescription());
            System.out.println(dictionary.get(w));
            return dictionary.get(w).getDescription();
        }
        System.out.println("Cuvantul " + w + " nu a fost gasit.");
        return null;
    }

    public void getAllWords() {
        Set set = dictionary.entrySet();
        Iterator it = set.iterator();
        while(it.hasNext()) {
            Map.Entry<Word, Definition> intraremap = (Map.Entry<Word, Definition>)it.next();
            System.out.println(intraremap.getKey().getName());
        }
    }

    public void getAllDefinitions() {
        Set set = dictionary.entrySet();
        Iterator it = set.iterator();
        while(it.hasNext()) {
            Map.Entry<Word, Definition> intrareInMap = (Map.Entry<Word,Definition>) it.next();
            System.out.println(intrareInMap.getValue().getDescription());
        }
    }
}