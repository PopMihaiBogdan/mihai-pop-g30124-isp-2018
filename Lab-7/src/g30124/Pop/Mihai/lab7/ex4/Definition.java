package g30124.Pop.Mihai.lab7.ex4;

public class Definition {

    private String description;

    Definition(String description){
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}