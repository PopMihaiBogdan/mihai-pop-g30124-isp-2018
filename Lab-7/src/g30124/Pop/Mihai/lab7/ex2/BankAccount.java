package g30124.Pop.Mihai.lab7.ex2;

public class BankAccount {
    String owner;
    double balance;

    public void withdraw(double amount){
        balance-=amount;
    }

    public void deposit(double amount){
        balance+=amount;
    }

    public BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }

    public String getOwner(){
        return this.owner;
    }

    public double getBalance(){
        return this.balance;
    }

    public boolean equals(Object obj) {
        if(obj instanceof BankAccount){
            BankAccount b = (BankAccount)obj;
            return balance == b.balance;
        }
        return false;
    }
}
