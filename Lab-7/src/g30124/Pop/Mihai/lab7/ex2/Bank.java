package g30124.Pop.Mihai.lab7.ex2;

import java.util.*;

public class Bank {
    ArrayList<BankAccount> BankAccounts = new ArrayList();

    public void addAccount(String owner, double balance){
        BankAccount b1 = new BankAccount(owner,balance);
        BankAccounts.add(b1);
    }

    public void printAccounts(){
        for (int i = 0; i < BankAccounts.size(); i++)
            System.out.println(BankAccounts.get(i).getOwner()+" "+BankAccounts.get(i).getBalance());
    }

    public void printAccounts(double minBalance, double maxBalance){
        Iterator<BankAccount> i=BankAccounts.iterator();

        while((i.hasNext())&&(i.next().getBalance() >= minBalance)&&(i.next().getBalance() <= maxBalance))
            System.out.println(i.next().getOwner()+" "+i.next().getBalance());
    }

    public BankAccount getAccount(String owner){
        Iterator<BankAccount> i=BankAccounts.iterator();
        while(i.hasNext())
            if(i.next().getOwner() == owner){
                return i.next();
            }
        return null;
    }

    public void sortbyName(){
        for(int i = 0; i < BankAccounts.size()-1; i++)
            for(int j = 1; j <= i+1; j++)
            if(BankAccounts.get(i).getOwner().compareTo( BankAccounts.get(j).getOwner())>0) {
                Collections.swap(BankAccounts, i, j);
            }
    }

    public static void main(String[] args) {
        Bank b = new Bank();
        b.addAccount("Dan",10000);
        b.addAccount("Alin",5000);
        b.addAccount("Cosmin",500);
        b.sortbyName();
        b.printAccounts();
    }
}