package g30124.Pop.Mihai.lab7.ex3;

import java.util.Comparator;
import g30124.Pop.Mihai.lab7.ex2.BankAccount;

class SortbyOwner implements Comparator<BankAccount> {
    @Override
    public int compare(BankAccount b1, BankAccount b2) {
        return b1.getOwner().toUpperCase().compareTo(b2.getOwner().toUpperCase());
    }
}