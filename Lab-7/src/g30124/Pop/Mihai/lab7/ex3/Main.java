package g30124.Pop.Mihai.lab7.ex3;

public class Main {
    public static void main(String[] args) {
        BankWithTree bank = new BankWithTree();
        bank.addAccount("Owner1", 10);
        bank.addAccount("Owner2", 20);
        bank.addAccount("Owner3", 30);
        bank.addAccount("Owner4", 40);
        bank.printAccounts();
        bank.printAccounts(20, 40);
        bank.addAccount("Owner5", 50);
        bank.addAccount("Owner6", 60);
        bank.printAccounts(20,50);
        bank.getAccount("Owner7");
        bank.getAllAcounts();

    }

}
