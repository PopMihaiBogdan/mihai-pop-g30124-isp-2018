package g30124.Pop.Mihai.lab7.ex1;

public class BankAccount {
    String owner;
    double balance;

    public void withdraw(double amount){
        balance-=amount;
    }

    public void deposit(double amount){
        balance+=amount;
    }

    BankAccount(String owner, double balance){
        this.owner=owner;
        this.balance=balance;
    }
    public boolean equals(Object obj) {
        if(obj instanceof BankAccount){
            BankAccount b = (BankAccount)obj;
            return balance == b.balance;
        }
        return false;
    }
    public static void main(String[] args) {
        BankAccount b1= new BankAccount("Alin",10000);
        BankAccount b2 = new BankAccount("Dan",5000);
        if(b1.equals(b2))

            System.out.println(b1+" and "+b2+ " are equals");
        else
            System.out.println(b1+" and "+b2+ " are NOT equals");
    }
}
