package g30124.pop.mihai.l3.e4;

import becker.robots.*;

public class ex4 {
	
	public static void main(String[] args)
	{
		City Londra = new City();
		Robot John = new Robot(Londra, 0, 2, Direction.WEST);
		Wall b0 = new Wall(Londra,1,1,Direction.NORTH);
		Wall b1 = new Wall(Londra,1,2,Direction.NORTH);
		Wall b2 = new Wall(Londra,1,2,Direction.EAST);
		Wall b3 = new Wall(Londra,2,2,Direction.EAST);
		Wall b4 = new Wall(Londra,2,2,Direction.SOUTH);
		Wall b5 = new Wall(Londra,2,1,Direction.SOUTH);
		Wall b6 = new Wall(Londra,1,1,Direction.WEST);
		Wall b7 = new Wall(Londra,2,1,Direction.WEST);
		John.move();
		John.move();
		John.turnLeft();
		John.move();
		John.move();
		John.move();
		John.turnLeft();
		John.move();
		John.move();
		John.move();
		John.turnLeft();
		John.move();
		John.move();
		John.move();
		John.turnLeft();
		John.move();
		
		
	}
}
