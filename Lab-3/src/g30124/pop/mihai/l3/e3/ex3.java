package g30124.pop.mihai.l3.e3;

import becker.robots.*;

public class ex3 {

	public static void main(String[] args)
	{
		City ny = new City();
		Robot John = new Robot(ny, 1, 1, Direction.NORTH);
		John.move();
		John.move();
		John.move();
		John.move();
		John.move();
		
		John.turnLeft();
		John.turnLeft();
		
		John.move();
		John.move();
		John.move();
		John.move();
		John.move();
	}
}
