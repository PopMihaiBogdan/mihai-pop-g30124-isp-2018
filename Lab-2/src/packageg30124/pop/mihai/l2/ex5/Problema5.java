package packageg30124.pop.mihai.l2.ex5;

import java.util.Scanner;

public class Problema5 {
 private static Scanner s;

public static void main(String []args) {
   int num, i, j, temp;
   s = new Scanner(System.in);

   System.out.println("Introduceti numarul de nr. pentru sortare:");
   num = s.nextInt();

   int array[] = new int[num];

   System.out.println("Enter " + num + " element: ");

   for (i = 0; i < num; i++) 
     array[i] = s.nextInt();

   for (i = 0; i < ( num - 1 ); i++) {
     for (j = 0; j < num - i - 1; j++) {
       if (array[j] > array[j+1]) 
       {
          temp = array[j];
          array[j] = array[j+1];
          array[j+1] = temp;
       }
     }
   }

   System.out.println("Lista sortata:");

   for (i = 0; i < num; i++) 
     System.out.println(array[i]);
 }
}