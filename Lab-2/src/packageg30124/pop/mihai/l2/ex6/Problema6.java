package packageg30124.pop.mihai.l2.ex6;

import java.util.Scanner;
public class Problema6 {
	

	static int fact(int n){
		int i,s=1;
		for(i=1; i<=n;i++)
			s*=i;
		return s;
	}
	

	static int fact1(int n){
		if (n==1) return 1;
		else return n*fact1(n-1);
	}
	
	public static void main(String[] args){
		int n;
		
		Scanner in = new Scanner(System.in);
		System.out.println("Dati n= ");
		n = in.nextInt();
		in.close();
		
		System.out.println("Factorialul lui n1: "+fact(n));
		System.out.println("Factorialul lui n2: "+fact1(n));
	}
}
