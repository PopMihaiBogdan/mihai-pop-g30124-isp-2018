package packageg30124.pop.mihai.l2.ex4;

import java.util.Scanner;
public class Problema4 
{
    private static Scanner s;

	public static void main(String[] args) 
    {
        int n, max;
        
        s = new Scanner(System.in);
        
        System.out.print("Da-ti numarul de elemente din vector");
        n = s.nextInt();
        
        int a[] = new int[n];
        
        System.out.println("Da-ti elementele");
        
        
        for(int i = 0; i < n; i++)
        {
            a[i] = s.nextInt();
        }
        max = a[0];
        
        
        for(int i = 0; i < n; i++)
        {
            if(max < a[i])
            {
                max = a[i];
            }
        }
        System.out.println("Valoare maxima:"+max);
    }
}
