package g30124.pop.mihai.l4.e4;

public class Author {
	private String name;
	private String email;
	private char g;
	public Author(String name,String email,char g)
	{
		this.name = name;
		this.email= email;
		this.g= g;
	}
	
	void setEmail(String email){
		this.email = email;
	}
	
	public String getName(){
		return name;
	}
	
	String getEmail(){
		return email;
	}
	
	char getGender(){
		return g;
	}
	
	public String toString(){
		return this.name + " (" + this.g +") at " + this.email;
	}
	
	public static void main(String[] args){
		Author a = new Author("J.K.Rowling","J.K.Rowling@yahoo.com",'f');
		Author b = new Author("Joules.Verne","Joule.Verne@yahoo.com",'m');
		System.out.println(a.getName() + " " + a.getEmail());
		System.out.println(a.toString());
	}
}