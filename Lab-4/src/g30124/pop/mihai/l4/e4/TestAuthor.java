package g30124.pop.mihai.l4.e4;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {
	@Test
public void shouldCreateaName(){
	Author a = new Author("J.K.Rowling","J.K.Rowling@gmail.com",'f');
	 assertEquals(a.getName(), "J.K.Rowling",0.01);	
}
	@Test
public void shouldCreateaEmail(){
	Author a = new Author("J.K.Rowling","J.K.Rowling@gmail.com",'f');
	assertEquals(a.getEmail(), "J.K.Rowling@gmail.com",0.01);	
}
	@Test
public void shouldChangeaEmail(){
	Author a = new Author("J.K.Rowling","J.K.Rowling@gmail.com",'f');
	a.setEmail("J.K.Rowling@yahoo.com");
	assertEquals(a.getEmail(), "J.K.Rowling@yahoo.com",0.01);	
}
	@Test
public void shouldCreateaGender(){
	Author a = new Author("J.K.Rowling","J.K.Rowling@gmail.com",'f');
	assertEquals(a.getGender(), 'f',0.01);	
}
	@Test
public void shouldShowToString(){
	Author a = new Author("J.K.Rowling","J.K.Rowling@gmail.com",'f');
	assertEquals(a.toString(), "J.K.Rowling (f) at J.K.Rowling@gmail.com ",0.01);	
}

}