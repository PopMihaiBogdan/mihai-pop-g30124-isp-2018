package g30124.pop.mihai.l4.e8;

public class Rectangle {
	Shape a;
	private double width;
	private double length;
	Rectangle(){
		this.width = 1;
		this.length = 1;
	}
	Rectangle(double width,double length){
		this.width = width;
		this.length = length;
	}
	Rectangle(double width,double length,String color,boolean filled){
		this.width = width;
		this.length = length;
		a.setColor(color);
		a.setFilled(filled);
	}
	public void setWidth(double width){
		this.width=width;
	}
	public double getWidth(){
		return width;
	}
	public void setLength(double length){
		this.length=length;
	}
	public double getLength(){
		return length;
	}
	public double getArea(){
		return length*width;
	}
	public double getPerimetre(){
		return 2*(length + width);
	}
	public String toString(){
		return " ";
	}
}