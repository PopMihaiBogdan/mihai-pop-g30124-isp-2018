package g30124.pop.mihai.l4.e8;

public class Shape {
	boolean filled;
	String color;
Shape(){
	filled = true;
	color = "red";
}
Shape(String color,boolean filled){
	this.color=color;
	this.filled=filled;
}
public void setColor(String color){
	this.color = color;
}
 public String getColor(){
	 return color;
 }
 public void setFilled(boolean filled){
	 this.filled = filled;
 }
 public boolean isFilled(){
	 return filled;
 }
public String toString(){
	return " "; 
 }
 
 
}