package g30124.pop.mihai.l4.e5;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import g30124.pop.mihai.l4.e4.Author;
public class TestBook {
@Test 
public void shouldBeSameAuthor(){
	Author a = new Author("J.K.Rowling","J.K.Rowling@gmail.com",'m');
	Book b = new Book("HarryPotter", a ,10);
	assertEquals(a.getName(),b.getAuthor().getName(),0.01);
}
@Test 
public void shouldChangeTheQTY(){
	Author a = new Author("J.K.Rowling","J.K.Rowling@gmail.com",'m');
	Book b = new Book("HarryPotter", a ,10,50);
	b.setQytInStock(99);
	assertEquals(b.getQytInStock(),100,0.01);
}

}