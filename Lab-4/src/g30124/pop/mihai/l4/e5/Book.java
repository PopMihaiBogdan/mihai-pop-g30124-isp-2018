package g30124.pop.mihai.l4.e5;

import g30124.pop.mihai.l4.e4.Author;

public class Book {

	private String name;
	private Author a;
	private int price;
	private int qtyInStock;
	public Book(String name,Author a,int price){
		this.name = name;
		this.a = a;
		this.price = price;
	}
	
	public Book(String name,Author a,int price,int qtyInStock){
		this.name = name;
		this.a = a;
		this.price = price;
		this.qtyInStock = qtyInStock;
	}
	

	public String getName(){
		return this.name;
	}
	
	public Author getAuthor(){
		 return a;
	 }
	
	public int getPrice(){
		return this.price; 
	 }
	 
	public void serPrice(int price){
		 this.price = price;
	 }
	 
	public void setQytInStock(int qis) {
		this.qtyInStock = qis;
	 }
	
	public  int getQytInStock(){
		 return this.qtyInStock;
	 }
	

}