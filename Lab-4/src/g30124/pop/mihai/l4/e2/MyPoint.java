package g30124.pop.mihai.l4.e2;

public class MyPoint {
    private int x,y;
    
    public MyPoint()
    {
        x=0;
        y=0;
    }
    public MyPoint(int x1,int y1){
        x=x1;
        y=y1;
    }
    public void setX(int x1){
    	x=x1;
    }
    public void setY(int y1){
        y=y1;
    }
    public int getX() {
    	return x;
    }

    public int getY() {
    	return y;
    }
    public void setXY(int x1,int y1){
        x=x1;
        y=y1;
    }
    public String toString(){
    	return "("+getX()+","+getY()+")";
    }
    public double distance(int x1, int y1){
        return Math.round(Math.sqrt( Math.pow(x1-x,2)+Math.pow(y1-y,2) )*1000.0)/1000.0;
    }
    public double distance (MyPoint p2){
        return Math.round(Math.sqrt(Math.pow(p2.getX()-x,2)+Math.pow(p2.getY()-y,2))*100.0)/100.0;

    }
}