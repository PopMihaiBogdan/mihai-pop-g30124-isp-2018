package g30124.pop.mihai.l4.e2;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestMyPoint {
    private MyPoint p1;    
    public void setUp()
    {
        p1=new MyPoint();
    }    
    public void testSetx(){
        p1.setX(6);
        assertEquals(p1.getX(),6);
    }
    public void testSety(){
        p1.setY(10);
        assertEquals(p1.getY(),10);
    }
    public void testSetxy() {
        p1.setXY(2,3);
        assertEquals(p1.getX(),2);
        assertEquals(p1.getY(),3);
    }
    public void testDistance(){
        p1.setXY(2,3);
        assertEquals(p1.distance(2,6),3.0);
    }
    public void testDistancePoint(){
        p1.setXY(2,3);
        MyPoint p2=new MyPoint(2,6);
        assertEquals(p1.distance(p2),3.0);
    }
}