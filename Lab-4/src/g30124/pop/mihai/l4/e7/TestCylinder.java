package g30124.pop.mihai.l4.e7;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class TestCylinder {
@Test
public void shouldGetVolume(){
	Cylinder a = new Cylinder(4,5);
	assertEquals(a.getVolume(),4*5*2*3.14,0.01);
}

@Test
public void shouldGetArea(){
	Cylinder a = new Cylinder(4,5);
	assertEquals(a.getArea(),2*3.14*4*(4+5),0.01);
}
}