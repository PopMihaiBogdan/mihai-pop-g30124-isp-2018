package g30124.pop.mihai.l4.e7;

public class Cylinder {

	double height;
	Circle c = new Circle();
	Cylinder(){
		height=1;
	}
	
	Cylinder(double r){
		height=1;
		c = new Circle(r);
	}
	

	Cylinder(double radius, double height){
		c.setRadius(radius);
		this.height = height;
	}
	
	public double getHeight(){
		return height;
	}
	
	public double getVolume(){
		return 2*3.14*c.getRadius()*height;
	}
	
	public double getArea(){
		return c.getArea()*(height + c.getRadius());
	}
}