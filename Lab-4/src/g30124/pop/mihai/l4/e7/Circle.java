package g30124.pop.mihai.l4.e7;


public class Circle {
double radius;

public Circle(){
	radius = 1;
}

public Circle(double r){
	radius = r;
}

double getArea(){
	double aria;
	aria=2*3.14*radius;
	return aria;
}

public void setRadius(double radius){
	this.radius = radius;
}

double getRadius(){
	return radius;
}

public String toString(){
	return " ";
}


}