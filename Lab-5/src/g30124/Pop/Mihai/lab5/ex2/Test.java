package g30124.Pop.Mihai.lab5.ex2;


public class Test {


    public static void main(String[] args) {
        ProxyImage proxyImage=new ProxyImage("pic.jpg",false);
        ProxyImage proxyImage1=new ProxyImage("picture2.png",true);
        proxyImage.display();
        System.out.println("----");
        proxyImage1.display();
    }

}
