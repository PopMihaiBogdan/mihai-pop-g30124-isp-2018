package g30124.Pop.Mihai.lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    public int tempSensor;

    @Override
    public int readValue() {
        Random number=new Random();
        tempSensor=number.nextInt(100);
        return tempSensor;
    }


}
