package g30124.Pop.Mihai.lab8.ex1;


public class ConcentrationException extends Throwable {
    int c;
    public ConcentrationException(int c,String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}