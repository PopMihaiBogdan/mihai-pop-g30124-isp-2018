package g30124.Pop.Mihai.lab8.ex2;

import java.io.*;
import java.util.*;
import java.lang.String;
import java.util.Scanner;

public class Read {
    public static void main(String[] args) throws IOException {

        int counter = 0;
        BufferedReader br = null;
        String line;
        System.out.println("Give the char:");
        char c=(char) System.in.read();
     try {

            br = new BufferedReader(new FileReader("exercise1.txt"));

            while ((line = br.readLine()) != null) {
                int n=line.length();
                for(int i=0;i<n;i++)
                    if(line.charAt(i)==c)
                        counter++;
            }

        }
        catch(Exception e)
        {
            System.out.println("You got an error");
        }

        System.out.println("Number of "+c+" =" + counter);
    }
}

