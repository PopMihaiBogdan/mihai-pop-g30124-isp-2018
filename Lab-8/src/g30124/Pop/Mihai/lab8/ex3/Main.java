package g30124.Pop.Mihai.lab8.ex3;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void Encription(String fileName) throws IOException{

        BufferedReader bf=new BufferedReader(new FileReader(fileName));
        BufferedWriter wr=new BufferedWriter(new FileWriter("date.enc"));

        String line;
        while((line=bf.readLine())!=null)
        {
            int line_counter=line.length();

            for(int i=0;i<line_counter;i++)
            {
                char c=line.charAt(i);
                c++;
                wr.write(c);
            }
            wr.newLine();
        }
        wr.close();
    }
    public static void Decription (String fileName) throws IOException {
        BufferedReader bf=new BufferedReader(new FileReader(fileName));
        BufferedWriter wr=new BufferedWriter(new FileWriter("date.dec"));

        String line;
        while((line=bf.readLine())!=null)
        {
            int line_counter=line.length();

            for(int i=0;i<line_counter;i++)
            {
                char c=line.charAt(i);
                c--;
                wr.write(c);
            }
            wr.newLine();
        }
        wr.close();
    }
    public static void main(String[] args) throws IOException {
        Scanner in=new Scanner(System.in);
        String file="date.txt";
        String fileEnc="date.enc";
        System.out.println("Enter your choice \n 1:Encription \n 2:Decription \n 3:Exit");
        int choice=in.nextInt();
        while(choice!=3){
            if (choice==1){
                Encription(file);
                System.out.println("Files succesfully ecrypted");
            }
            else
                if (choice==2){
                Decription(fileEnc);
                    System.out.println("Files succesfully decrypted");
            }
            System.out.println("Next command:");
            choice=in.nextInt();

    }}
}
